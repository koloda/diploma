<?php

/* UserProfileBundle:Default:index.html.twig */
class __TwigTemplate_d8f678145317fd535204005e55ffc90c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'left_bar' => array($this, 'block_left_bar'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_left_bar($context, array $blocks = array())
    {
        echo "     
   <div class=\"span4\" id=\"left_bar\">
        <div id=\"profile_photo\">
            <img alt=\"\" src=\"/web/media/images/ava.jpg\">
        </div>
        
        <div id=\"left_profile_menu\" >
            
            <ul class=\"nav nav-list well\">
              <li class=\"nav-header\">
                List header
              </li>
              <li class=\"active\">
                <a href=\"#\">Home</a>
              </li>
              <li>
                <a href=\"#\">Library</a>
              </li>
            </ul>
            
        </div>
    </div>
    ";
    }

    // line 28
    public function block_content($context, array $blocks = array())
    {
        // line 29
        echo "    <div class=\"span8\" id=\"right_bar\">
        
        <div class=\"main_header\"></div>
        
        <form action=\"/web/microblog/";
        // line 33
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "user"), "email"), "html", null, true);
        echo "\" method=\"post\"";
        echo $this->env->getExtension('form')->renderEnctype($this->getContext($context, "form"));
        echo " id=\"microblog_new_message\">
        ";
        // line 34
        echo $this->env->getExtension('form')->renderErrors($this->getContext($context, "form"));
        echo "
        ";
        // line 35
        echo $this->env->getExtension('form')->renderRest($this->getContext($context, "form"));
        echo "
        <input type=\"submit\" id=\"sendMicroblogMessageButton\"  class=\"btn\"/>
        </form>
        
        ";
        // line 39
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "messages"));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 40
            echo "        
        <div class=\"wallMessage\">
        <div class=\"wallMessageAuthor\">
        ";
            // line 43
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "message"), "authorguid"), "html", null, true);
            echo "
        </div>
        ";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "message"), "body"), "html", null, true);
            echo "
        </div>
        
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 49
        echo "    </div>
    
    <script type=\"text/javascript\">
    /*
    \$(document).ready(function() {
        \$('#microblog_new_message').submit(function() {
            alert(1);
            ansver = \$().post('http://sf2');
            console.log(ansver);
            return true;
            })
        })
*/
    </script>
";
    }

    public function getTemplateName()
    {
        return "UserProfileBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }
}
