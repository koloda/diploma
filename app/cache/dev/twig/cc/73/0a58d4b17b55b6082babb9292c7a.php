<?php

/* UserProfileBundle:Default:base_profile_register.html.twig */
class __TwigTemplate_cc730a58d4b17b55b6082babb9292c7a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "
";
        // line 5
        if ($this->getAttribute($this->getContext($context, "message", true), "msg", array(), "any", true, true)) {
            // line 6
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "message"), "msg"), "html", null, true);
            echo "
";
        }
        // line 8
        echo "
<form action=\"\" method=\"post\"";
        // line 9
        echo $this->env->getExtension('form')->renderEnctype($this->getContext($context, "form"));
        echo ">
";
        // line 10
        echo $this->env->getExtension('form')->renderErrors($this->getContext($context, "form"));
        echo "
";
        // line 11
        echo $this->env->getExtension('form')->renderRest($this->getContext($context, "form"));
        echo "
<input type=\"submit\" class=\"btn\" />
</form>


<script type=\"text/javascript\">
    \$(\"#register_user_profile_birthdate\").datepicker();
</script>
";
    }

    public function getTemplateName()
    {
        return "UserProfileBundle:Default:base_profile_register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }
}
