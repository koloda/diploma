<?php

/* UserProfileBundle:Default:base_profile_edit.html.twig */
class __TwigTemplate_229ce0f370f1182c9273332e0999ab65 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'left_bar' => array($this, 'block_left_bar'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_left_bar($context, array $blocks = array())
    {
        echo "     
   <div class=\"span4\" id=\"left_bar\">
        <div id=\"profile_photo\">
            <img alt=\"\" src=\"/web/media/images/ava.jpg\">
        </div>
        
        <div id=\"left_profile_menu\" >
            
            <ul class=\"nav nav-list well\">
              <li class=\"nav-header\">
                List header
              </li>
              <li class=\"active\">
                <a href=\"#\">Home</a>
              </li>
              <li>
                <a href=\"#\">Library</a>
              </li>
            </ul>
            
        </div>
    </div>
    ";
    }

    // line 31
    public function block_content($context, array $blocks = array())
    {
        // line 32
        echo "
";
        // line 33
        if ($this->getAttribute($this->getContext($context, "message", true), "msg", array(), "any", true, true)) {
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "message"), "msg"), "html", null, true);
            echo "
";
        }
        // line 36
        echo "
<form action=\"\" method=\"post\"";
        // line 37
        echo $this->env->getExtension('form')->renderEnctype($this->getContext($context, "form"));
        echo ">
";
        // line 38
        echo $this->env->getExtension('form')->renderErrors($this->getContext($context, "form"));
        echo "
";
        // line 39
        echo $this->env->getExtension('form')->renderRest($this->getContext($context, "form"));
        echo "
<input type=\"submit\" class=\"btn\"/>
</form>

<script type=\"text/javascript\">
    \$(\"#base_user_profile_birthdate\").datepicker({ dateFormat: 'M d, yy'});
</script>
";
    }

    public function getTemplateName()
    {
        return "UserProfileBundle:Default:base_profile_edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }
}
