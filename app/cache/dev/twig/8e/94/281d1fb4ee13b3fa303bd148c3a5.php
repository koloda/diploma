<?php

/* UserMicroBlogBundle:Default:index.html.twig */
class __TwigTemplate_8e94281d1fb4ee13b3fa303bd148c3a5 extends Twig_Template
{
    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "Hello ";
        echo twig_escape_filter($this->env, $this->getContext($context, "name"), "html", null, true);
        echo "!
";
    }

    public function getTemplateName()
    {
        return "UserMicroBlogBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }
}
