<?php

namespace User\MicroBlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \DateTime;

/**
 * User\MicroBlogBundle\Entity\Microblogmessage
 *
 * @ORM\Table(name="MicroblogMessage")
 * @ORM\Entity
 */
class Microblogmessage
{
    /**
     * @var string $guid
     *
     * @ORM\Column(name="guid", type="string", length=36, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $guid;

//     /**
//      * @var integer $id
//      *
//      * @ORM\Column(name="id", type="integer", nullable=false)
//      */
//     private $id;

    /**
     * @var string $authorguid
     *
     * @ORM\Column(name="authorGuid", type="string", length=36, nullable=false)
     */
    private $authorguid;

    /**
     * @var string $ownerguid
     *
     * @ORM\Column(name="ownerGuid", type="string", length=36, nullable=false)
     */
    private $ownerguid;
    
    
    /**
     * @var text $body
     *
     * @ORM\Column(name="body", type="text", nullable=false)
     */
    private $body;

    /**
     * @var datetime $publishdate
     *
     * @ORM\Column(name="publishDate", type="datetime", nullable=false)
     */
    private $publishdate;

    /**
     * @var boolean $removed
     *
     * @ORM\Column(name="removed", type="boolean", nullable=false)
     */
    private $removed;


    
    public function __construct()
    {
        $this->guid = $this->createGuid();
        $this->publishdate = new DateTime();
        $this->removed = false;
    }

    /**
     * Set id
     *
     * @param integer $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set authorguid
     *
     * @param string $authorguid
     */
    public function setAuthorguid($authorguid)
    {
        $this->authorguid = $authorguid;
    }

    /**
     * Get authorguid
     *
     * @return string 
     */
    public function getAuthorguid()
    {
        return $this->authorguid;
    }
    
    /**
     * Set ownerguid
     *
     * @param string $ownerguid
     */
    public function setOwnerguid($ownerguid)
    {
        $this->ownerguid = $ownerguid;
    }
    
    /**
     * Get ownerguid
     *
     * @return string
     */
    public function getOwnerguid()
    {
        return $this->ownerguid;
    }

    /**
     * Set body
     *
     * @param text $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * Get body
     *
     * @return text 
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set publishdate
     *
     * @param datetime $publishdate
     */
    public function setPublishdate($publishdate)
    {
        $this->publishdate = $publishdate;
    }

    /**
     * Get publishdate
     *
     * @return datetime 
     */
    public function getPublishdate()
    {
        return $this->publishdate;
    }

    /**
     * Set removed
     *
     * @param boolean $removed
     */
    public function setRemoved($removed)
    {
        $this->removed = $removed;
    }

    /**
     * Get removed
     *
     * @return boolean 
     */
    public function getRemoved()
    {
        return $this->removed;
    }

    /**
     * Get guid
     *
     * @return string 
     */
    public function getGuid()
    {
        return $this->guid;
    }
    
    private function createGuid()
    {
    
        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
                // 32 bits for "time_low"
                mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
    
                // 16 bits for "time_mid"
                mt_rand( 0, 0xffff ),
    
                // 16 bits for "time_hi_and_version",
                // four most significant bits holds version number 4
                mt_rand( 0, 0x0fff ) | 0x4000,
    
                // 16 bits, 8 bits for "clk_seq_hi_res",
                // 8 bits for "clk_seq_low",
                // two most significant bits holds zero and one for variant DCE1.1
                mt_rand( 0, 0x3fff ) | 0x8000,
    
                // 48 bits for "node"
                mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
    
    }
}