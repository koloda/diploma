<?php

namespace User\MicroBlogBundle\Controller;

use User\MicroBlogBundle\Entity\Microblogmessage;

use User\MicroBlogBundle\Form\MicroblogmessageType;

use JMS\DiExtraBundle\Annotation\Validator;

use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Validator\Constraints\NotBlank;

use Symfony\Component\Validator\Constraints\Collection;

use Symfony\Component\HttpFoundation\Request;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("/microblog/{userEmail}", name="microblogNewMessage")
     * @Template()
     */
    public function indexAction($userEmail, Request $request)
    {
        $user = $this->get('security.context')->getToken()->getUser();
        
        $newConstraintsContainer = new Collection(array(
                'body'     => new NotBlank(),
                '_token'   => new NotBlank() 
                ));
        
        $newMicroblogMessage = new Microblogmessage();
        
        $microoblogForm = $this->createForm(new MicroblogmessageType(), $newMicroblogMessage);
        $microoblogForm->bindRequest($request);
        
        if ($userEmail === $user->getEmail())
            $ownerUser = $user;
        else
            $ownerUser = $this->getDoctrine()->getRepository('UserProfileBundle:Userprofile')
                ->findOneByEmail($userEmail);
        
        if ($microoblogForm->isValid())
        {
            $form_data = $request->request->get('user_microblogbundle_microblogmessagetype');
            
            $newMicroblogMessage->setBody($form_data['body']);
            $newMicroblogMessage->setAuthorguid($user->getGuid());
            $newMicroblogMessage->setOwnerguid($ownerUser->getGuid());
            
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($newMicroblogMessage);
            $em->flush();
            
            return $this->redirect("/web/user/$userEmail");
            
        }
        
        return new Response(0);
    }
}
