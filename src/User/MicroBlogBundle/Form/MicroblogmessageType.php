<?php

namespace User\MicroBlogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class MicroblogmessageType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('body', 'text', array('label' => ' Watsup?'))
        ;
    }

    public function getName()
    {
        return 'user_microblogbundle_microblogmessagetype';
    }
}
