<?php

namespace User\MicroBlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * User\MicroBlogBundle\Entity\MicroblogMessage
 *
 * @ORM\Table(name="MicroblogMessage")
 * @ORM\Entity(repositoryClass="User\MicroBlogBundle\Entity\MicroblogMessageRepository")
 */
class MicroblogMessage
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}