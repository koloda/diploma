<?php


namespace User\ProfileBundle\Entity;

use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;


/**
 * User\ProfileBundle\Entity\Userprofile
 *
 * @ORM\Table(name="UserProfile")
 * @ORM\Entity
 * @UniqueEntity("email")
 */
class Userprofile implements UserInterface
{
    /**
     * @var string $guid
     *
     * @ORM\Column(name="guid", type="string", length=36, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $guid;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=32)
     */
    private $name;

    /**
     * @var string $surname
     *
     * @ORM\Column(name="surname", type="string", length=32)
     */
    private $surname;

    /**
     * @var boolean $gender
     *
     * @ORM\Column(name="gender", type="boolean")
     */
    private $gender;

    /**
     * @var string $email
     * @ORM\Column(name="email", type="string", length=32,  nullable=false, unique=true)
     * @Assert\Email()
     */
    private $email;

    /**
     * @var string $password
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     */
    private $password;
    
    /**
     * @var string $salt
     *
     * @ORM\Column(name="salt", type="string", length=255, nullable=false)
     */
    private $salt;

    /**
     * @var datetime $registerDate
     *
     * @ORM\Column(name="register_date", type="datetime", nullable=false)
     */
    private $registerDate;

    /**
     * @var boolean $removed
     *
     * @ORM\Column(name="removed", type="boolean")
     */
    private $removed;

    /**
     * @var date $birthDate
     *
     * @ORM\Column(name="birth_date", type="date")
     */
    private $birthDate;


    private function createGuid()
    {
        
        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
                // 32 bits for "time_low"
                mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
    
                // 16 bits for "time_mid"
                mt_rand( 0, 0xffff ),
    
                // 16 bits for "time_hi_and_version",
                // four most significant bits holds version number 4
                mt_rand( 0, 0x0fff ) | 0x4000,
    
                // 16 bits, 8 bits for "clk_seq_hi_res",
                // 8 bits for "clk_seq_low",
                // two most significant bits holds zero and one for variant DCE1.1
                mt_rand( 0, 0x3fff ) | 0x8000,
    
                // 48 bits for "node"
                mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
    
    }

    public function __construct()
    {
        $this->salt = sha1(microtime());
        $this->guid = $this->createGuid(); 
    }
    
    /**
     * Get guid
     *
     * @return string 
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set surname
     *
     * @param string $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     * Get surname
     *
     * @return string 
     */
    public function getSurname()
    {
        return $this->surname;
    }
    
    public function getFullName()
    {
    	return $this->getName().' '.$this->getSurname();
    }

    /**
     * Set gender
     *
     * @param boolean $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * Get gender
     *
     * @return boolean 
     */
    public function getGender()
    {
        return $this->gender;
    }

    
    public function setEmail($email)
    {
        $this->email = $email;
    }
    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }
    
    /**
     * Set password
     * 
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $this->createPasswordHash($password);  
    }
    
    public function getSalt()
    {
    	return $this->salt;
    }
    
    public function setSalt($salt)
    {
    	$this->salt = $salt;
    }
    
    public function createPasswordHash($password)
    {
        $encoder = new MessageDigestPasswordEncoder('sha1', true, 1);
        return $this->password = $encoder->encodePassword($password, $this->getSalt() ) ;
    }

    public function setRegisterDate($registerdate)
    {
        $this->registerDate = $registerdate;
        $this->removed = false;
    }
    /**
     * Get registerDate
     *
     * @return datetime 
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * Get removed
     *
     * @return boolean 
     */
    public function getRemoved()
    {
        return $this->removed;
    }

    /**
     * Set birthDate
     *
     * @param date $birthDate
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;
    }

    /**
     * Get birthDate
     *
     * @return date 
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }
    
    /*    sequrity    */
    public function getRoles() 
    {
        return array('ROLE_ADMIN');
    }
    
    
    public function eraseCredentials() {
    // TODO: Auto-generated method stub
    
    }
    
    
    public function equals(UserInterface $user) 
    {
        return ( $this->getUsername() === $user->getUsername());
    }
    
    
    public function getUsername() 
    {
        return $this->email;
    }



    
}