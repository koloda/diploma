<?php

namespace User\ProfileBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class RegisterUserProfileType extends AbstractType
{
    
    /**
    * @param FormBuilder $builder
    * @param array $options
    */
    public function buildForm(FormBuilder $builder, array $options) 
    {
        $builder
            ->add('email', 'email')
            ->add('password', null)
            ->add('name', null, array('required'=>false))
            ->add('surname', null, array('required'=>false))
            ->add('gender', 'choice', array('choices' => array('male', 'famale'), 'required'=>false))
            ->add('birthdate', 'text', array('label' => 'Birth Date', 'required'=>false));
    }
    
    public function getName()
    {
        return 'register_user_profile';
    }

    public function getDefaultOptions(array $options)
    {
        return array(
                'data_class' => 'User\ProfileBundle\Entity\Userprofile');
    }

}
