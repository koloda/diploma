<?php

namespace User\ProfileBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class BaseUserProfileType extends AbstractType
{
    
    /**
    * @param FormBuilder $builder
    * @param array $options
    */
    public function buildForm(FormBuilder $builder, array $options) 
    {
        $builder
            ->add('name')
            ->add('surname')
            ->add('gender', 'choice', array('choices' => array('male', 'famale')))
            ->add('birthdate', 'date', array('input' => 'datetime', 'widget' => 'single_text', 'label' => 'Date of birth'));//todo: check date field on work
    }
    
    public function getName()
    {
        return 'base_user_profile';
    }

    public function getDefaultOptions(array $options)
    {
        return array(
                'data_class' => 'User\ProfileBundle\Entity\Userprofile');
    }

}
