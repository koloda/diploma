<?php

namespace User\ProfileBundle\Controller;

use Symfony\Component\Security\Core\SecurityContext;

use Symfony\Component\Validator\Constraints\NotNull;

use Symfony\Component\Validator\Constraints\Email;

use User\ProfileBundle\Form\RegisterUserProfileType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use User\ProfileBundle\Entity\Userprofile;

use JMS\DiExtraBundle\Annotation\Validator;

use Symfony\Component\Validator\Constraints\Date;

use Symfony\Component\Validator\Constraints\MinLength;

use Symfony\Component\Validator\Constraints\NotBlank;

use Symfony\Component\Validator\Constraints\Collection;

use Symfony\Component\HttpFoundation\Request;

use User\ProfileBundle\Form\BaseUserProfileType;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use \DateTime;

class DefaultController extends Controller
{
    
    /**
     * @Route("/login", name="_demo_login")
     */
    public function loginAction()
    {
        if ($this->get('request')->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $this->get('request')->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $this->get('request')->getSession()->get(SecurityContext::AUTHENTICATION_ERROR);
        }
    
        $tpl_data =  array(
                'last_username' => $this->get('request')->getSession()->get(SecurityContext::LAST_USERNAME),
                'error'         => $error,
        );
        
        return $this->render('UserProfileBundle:Default:login.html.twig', $tpl_data);
    }
    
    /**
     * @Route("/login_check", name="_security_check")
     */
    public function securityCheckAction()
    {
        // The security layer will intercept this request
    }
    
    /**
     * @Route("/logout", name="_demo_logout")
     */
    public function logoutAction()
    {
        // The security layer will intercept this request
    }
    
    
    
    /**
     * @Route("/user", name="viewUserProfile")
     */
    public function indexAction()
    {
        $tpl_data = array();
    	$tpl_data['user'] = $this->get('security.context')->getToken()->getUser();
    	
        return $this->render('UserProfileBundle:Default:index.html.twig', $tpl_data);
    }
    
    /**
     * @Route("/register", name="registerUserProfile")
     */
    public function registerUserProfile(Request $request)
    {
        $form = $this->createForm(new RegisterUserProfileType());
        $tpl_data = array(
                'message' => array(),
                'errors'  => array()
        );
        
        
        if ($request->getMethod() === 'POST')
        {
            $newConstraintCollection = new Collection(array(
                    'email'       => array(new Email()),
                    'password'    => array(new MinLength(6)),
                    'name'        => array(new MinLength(3)),
                    'surname'     => array(new MinLength(3)),
                    'gender'      => new NotNull(),
                    'birthdate'   => new NotNull(),
                    '_token'      => new NotBlank()
            ));
            
            $errorList = $this->get('validator')->validateValue($request->get("register_user_profile"), $newConstraintCollection);
            
            $form->bindRequest($request); //TODO: optimize
            
            if ($form->isValid() && count($errorList) == 0)
            {
                $form_data = $request->get("register_user_profile");
                
                $user = new Userprofile();
                $user->setName($form_data['name']);
                $user->setSurname($form_data['surname']);
                $user->setEmail($form_data['email']);
                $user->setGender($form_data['gender']);
                $user->setPassword($form_data['password']);
                $user->setBirthDate(new DateTime());
                $user->setRegisterDate(new DateTime());
//                 $user->setBirthDate($form_data['birthdate']);    //TODO: need method to convert date to obj
                
                //TODO: later make email confirmation
                
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($user);
                $em->flush();
            
                $tpl_data['message']['msg'] = 'You registered ssuccess';
                $tpl_data['message']['type'] = 'info';
            }
            else
            {
                var_dump($errorList);
                
                $tpl_data['message']['msg'] = 'Reegistration error';
                $tpl_data['message']['type'] = 'error';
            }
            
        }
        
        $tpl_data['form'] = $form->createView();
        return $this->render('UserProfileBundle:Default:base_profile_register.html.twig', $tpl_data);
    }
    
    /**
     * @Route("/edit_profile", name="editUserProfile")
     */
    public function editProfileAction(Request $request)
    {
        /**/
        $em = $this->getDoctrine()->getEntityManager();
        $repo = $this->getDoctrine()->getRepository('UserProfileBundle:Userprofile');
        $user = $repo->find('ef4a7cf4-5cec-11e1-84d2-b870f41464f0');
        
        /**/
        
        
        $form = $this->createForm(new BaseUserProfileType(), $user);
        $tpl_data = array(
                'message' => array(),
                'errors'  => array()
                ); 
        
        if ($request->getMethod() === 'POST')
        {
            $newConstraintCollection = new Collection(array(
                    'name'        => array(new NotBlank(), new MinLength(3)),
                    'surname'     => array(new NotBlank(), new MinLength(3)),
                    'gender'      => new NotBlank(),
                    'birthdate'   => array(new NotBlank()),
                    '_token'      => new NotBlank()           
                    ));

            $errorList = $this->get('validator')->validateValue($request->get("base_user_profile"), $newConstraintCollection);
            
            $form->bindRequest($request); //TODO: optimize
                        
            if ($form->isValid() && count($errorList) == 0)
            {
                $em->persist($user);
                $em->flush();
                
                $tpl_data['message']['msg'] = 'Data saved success';
                $tpl_data['message']['type'] = 'info';
            }
            else
            {
                $tpl_data['message']['msg'] = 'Data saving error';
                $tpl_data['message']['type'] = 'error';
            }
        }

        $tpl_data['form'] = $form->createView();
    	return $this->render('UserProfileBundle:Default:base_profile_edit.html.twig', $tpl_data);
    	
    }
    
}
