-- phpMyAdmin SQL Dump
-- version 3.4.7.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 19, 2012 at 08:30 PM
-- Server version: 5.5.22
-- PHP Version: 5.3.10-1ubuntu3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `symfony`
--

-- --------------------------------------------------------

--
-- Table structure for table `MicroblogMessage`
--

CREATE TABLE IF NOT EXISTS `MicroblogMessage` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `guid` varchar(36) NOT NULL,
  `authorGuid` varchar(36) NOT NULL,
  `body` text NOT NULL,
  `publishDate` datetime NOT NULL,
  `removed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `MicroblogMessage`
--

INSERT INTO `MicroblogMessage` (`id`, `guid`, `authorGuid`, `body`, `publishDate`, `removed`) VALUES
(25, '8a36d6e7-8227-4ed0-b44b-f13afc1f61a4', '1e7378a4-d70a-4679-9c21-144ba89ef445', 'This is Vova''s first message on his Wall. This message was writed by Koloda. apr 6, 2012 - 02:00', '2012-04-06 02:00:24', 0);

-- --------------------------------------------------------

--
-- Table structure for table `UserProfile`
--

CREATE TABLE IF NOT EXISTS `UserProfile` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `guid` varchar(36) NOT NULL,
  `name` varchar(32) NOT NULL,
  `surname` varchar(32) NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `email` varchar(32) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) NOT NULL,
  `register_date` datetime NOT NULL,
  `removed` tinyint(1) NOT NULL DEFAULT '0',
  `birth_date` date NOT NULL,
  PRIMARY KEY (`guid`),
  UNIQUE KEY `id` (`id`),
  KEY `email` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `UserProfile`
--

INSERT INTO `UserProfile` (`id`, `guid`, `name`, `surname`, `gender`, `email`, `password`, `salt`, `register_date`, `removed`, `birth_date`) VALUES
(1, 'ef4a7cf4-5cec-11e1-84d2-b870f41464f0', 'koloda2', 'koloda', 0, 'koloda90@gmail.com', 'pass', 'salt', '2012-02-22 00:00:00', 0, '1990-09-03'),
(7, '05f41f4a-db6d-4e63-9684-226253596784', 'kjkjkj', 'kjkjkj', 0, 'koloda@kjl.cc', 'fa888c037d063755247e182d7a41cc56ba034ff3', '3182951813ed90e16a1114e7817a071b2695', '2012-04-04 23:51:16', 0, '2012-04-04'),
(6, 'd46a1611-dbf6-4cb7-ae54-43c77f3c924f', 'kjkjkj', 'kjkjkj', 0, 'koloda@kjl.cc', 'cb794f50e6c1454e663a4f4ab2cf0cb67a3c8e11', '132bb0f09cfb11aa5a7f34b7e5761facd0cb', '2012-04-04 23:51:00', 0, '2012-04-04'),
(8, '3d40f370-6a88-4b3b-aa10-6eef732af6be', 'kolo90da', 'kolo90da', 0, 'koloda@g.cc', 'a08954c9b725651f32a2f5407131f3af797369bf', 'e5a6aab7ced9f148596e70d28989319c2fb8', '2012-04-05 00:00:15', 0, '2012-04-05'),
(9, 'b43c9fd9-7111-4119-9ca9-32f39b635c1f', 'kolo90da', 'kolo90da', 0, 'koloda@g.cc', '9d31fd27964a3c6f6d3f0956f966ff4bafe68d1f', '996870e9c528c7e481e6c32f150904e0db78', '2012-04-05 00:00:20', 0, '2012-04-05'),
(10, '8c9e712c-823f-497f-95fd-bb5efba6b05b', 'kolo90da', 'kolo90da', 0, 'koloda@g.cc', '5a17c265bb7cc58ee96652cae9934d2811971095', '48e2a4e65f45ae4d4da3a0a5b4695d3b58de', '2012-04-05 00:00:22', 0, '2012-04-05'),
(11, 'dbc3eda2-f3c9-4342-8a06-53bc0af2dd83', 'ldskjldskj', 'sdlkjsldf', 1, 'koloda90@gmail1.com', '700646c639ea865166bc7d93e3e34f66763bcf2b', 'a321d190f61bbd5f262cbab5de7d84ff2836', '2012-04-05 01:26:28', 0, '2012-04-05'),
(12, '908f33d5-d80c-4270-84f3-99fb4952b7c7', '', '', 0, 'kosmos@n.cc', '4013864845854bb094c811624f4831d722baceca', '6e0d7af992d9bacb401439cdde768561e479', '2012-04-05 01:44:21', 0, '2012-04-05'),
(13, '071621c9-ad6f-4a42-8c26-857c7dbfb74f', 'koko', 'dddko', 0, 'koko@m.cc', '4c173d9fef67345e27fde33eb892dc9ff0f63046', 'b1a42789aceed3d24c010c76d8bb57497691', '2012-04-05 02:32:24', 0, '2012-04-05'),
(14, 'cc8ae8e1-b386-48f3-9e97-9ad654bf5fa0', '', '', 0, 'vova@m.cc', 'Yl55ywKVcmyKdUrdsHf8F8Fr9pc=', 'f26f12568599c80fc2d886f991bbff03c8fd', '2012-04-05 02:39:39', 0, '2012-04-05'),
(15, 'a30ef350-75b4-4ca3-ac93-a79837c2a013', '', '', 0, 'mm@m.cc', 'DtuPJYGT1pyW1nLndi2tf/eYtx8=', 'bca061c0d63e1db4817b84d4e4b2e1051c78', '2012-04-05 02:42:13', 0, '2012-04-05'),
(16, '9baf7f4a-830b-4ca5-8255-0dc100e2379e', '', '', 0, 'mm@m.ccc', 'mWe63Dp4HXfTWXG//tki4S5QJxk=', '10caca3d60f61b26240430bb37016cfa146aa1c1', '2012-04-05 02:49:20', 0, '2012-04-05'),
(17, '37248fc2-2570-4784-9085-96fd6ede54e8', '', '', 0, 'cccc@c.c', '8ca7c4b86488cae6a893b1f14c06bf096a24525d', 'd74f3fe97518e3a59e922771cdf5cec99dc0179b', '2012-04-05 02:55:43', 0, '2012-04-05'),
(18, '41640191-6556-48e8-8445-8e1dc76e13fa', '', '', 0, 'cccc@c.cc', 'vhaQBQapzx1tvgsJI44W1r3bYao=', '502af46fc5ba578329d0020b2f43518a7bd8d678', '2012-04-05 02:57:57', 0, '2012-04-05'),
(19, 'a0ed05db-105a-4603-9f74-598781945fd7', '', '', 0, 'cccc@c.ccc', 'mmmmmm', '3175def28baa4503c38701ea42f65e8921519bdb', '2012-04-05 03:00:05', 0, '2012-04-05'),
(20, '1b6c63ac-3bf5-44ae-a8b3-154b63e65b93', '', '', 0, 'aaa@c.ccc', '1DZT19ExNhTvJv+XbwUkKC6q98I=', '2f60fc024259ed4a2f4028ce0d4718d1ee124430', '2012-04-05 03:01:41', 0, '2012-04-05'),
(21, 'b4be1266-3e30-4df9-bbe2-8d892cd30b91', '', '', 0, 'kolo@m.cc', 'XQhxBcsJatMWRxCk4ea0VqPBhR0=', 'dc56cbd501b109e2c3b8d0b3fe10cf0d824f893e', '2012-04-05 03:12:20', 0, '2012-04-05'),
(22, '990811fb-a53d-438a-8085-f9ebbfb7d49f', '', '', 0, 'kos@s.ss', 'uW7TcRgl64XeCDIG7xZEL76cUwE=', 'b42a64c56a7ef44abe7ed6468e8863d9fba90737', '2012-04-05 22:51:57', 0, '2012-04-05'),
(23, '1e7378a4-d70a-4679-9c21-144ba89ef445', 'Vova', '', 0, 'durex2@rambler.ru', 'WTUvvw7o8uuQDSVMF9sMCkqji84=', '9bf1a62e57c3d4d0fd6987cd972833b2ef2d59b8', '2012-04-06 01:17:34', 0, '2012-04-06');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
